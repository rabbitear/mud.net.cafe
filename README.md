# MUD at _mud.net.cafe_
## == 2023-09-21 21:39:35 ==

## TODO:
    - put wiki on *play*
    - make a gitlab repo, public, for mud.net.cafe
    - upload .wiki/MUD.md as README.md

## Notes:
all the notes will be to be in this one text file, until somebody says
we have to add other files about other notes.


## The Same Page
This is the same page.  I'll write some instructions on how to put
together the envirnoment so you'll have everything installed to run,
test, code, on the MUD game.

## Instructions:
1) clone repo:
    - `git clone 'https://gitlab.com/rabbitear/mud.net.cafe.git`
    - `cd mud.net.cafe`
2) run venv (virtual env) in that:
    - `python3 -m venv venv`
    - `source venv/bin/activate`
1) Install with pip
    - `pip install evennia[extra]`
2) intialize a game:
    - `evennia --init mygame`
3) create default database:
    - `cd mygame`
    - `evennia migrate` -- creates mygame/server/evennia.db3
4) Start it:
    - `evennia start`
    - *We choose a superuser/god name and password*
5) Browse to `localhost:4005`

## Start the MUD sometime later
1) goto mygame directory in mud.net.cafe repo:
    - `cd somewhere/mud.net.cafe/mygame`
2) activate the venv:
    - `venv/bin/activate`
3) Start evennia:
    - `evennia start`
## Original Evennia Docs:
I try to follow the original docs to get the MUDs up and running, and
making a template to code games:
- https://www.evennia.com/docs/latest/Setup/Setup-Overview.html


